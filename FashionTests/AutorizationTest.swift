//
//  AutorizationTest.swift
//  FashionTests
//
//  Created by Alexandr Shevchenko on 05.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import XCTest
@testable import Fashion
class AutorizationTest: XCTestCase {
    
    var api: ApplicationAPI!
    override func setUp() {
        api = ApplicationAPI()
    }

    override func tearDown() {
        api = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testAvtorization(){
        api.autorization(login: "admin", password: "admin") { (success) in
            print("Success: \(success)")
        }
    }

}
