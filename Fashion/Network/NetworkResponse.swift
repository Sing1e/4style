//
//  NetworkResponse.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 13.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
struct Wardrobes: Codable {
    var id: Int
    var name: String
    var logoUrl: String
    var userId: Int
}

struct Cloth: Codable {
    var id: Int
    var name: String?
    var available: Bool?
    var favourite: Bool?
//    var imageContentType: String
    var image: String
    var clothTypeName: String
    var wardrobeId: Int?
    var clothTypeId: Int?
    var wardrobeName: String?
}

struct Style: Codable {
    var name: String
    var created: String
    var userId: Int?
    var cloths: [Cloth]
}

//Articless
struct Articles: Codable{
    var articles: [Article]
}
struct Article: Codable{
    var articleName: String
    var boundingBox: BoundingBox
    
    private enum CodingKeys: String, CodingKey{
        case articleName = "article_name"
        case boundingBox = "bounding_box"
    }
}
struct BoundingBox: Codable {
    var x0: Int
    var x1: Int
    var y0: Int
    var y1: Int
}
