//
//  NetworkRequest.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 08.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation

struct LoginRequest: Codable {
    var username: String
    var password: String
    var environmentInfo = false
}

struct LoginResponce: Codable {
    var token: String
    private enum CodingKeys: String, CodingKey {
        case token = "id_token"
    }
}

struct ClothRequest: Codable {
    var available: Bool
    var clothTypeId: Int
    var favourite: Bool
    var image: String
    var wardrobeId: Int
    var imageContentType: String = "image/jpeg"
}
