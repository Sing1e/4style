//
//  ApplicationAPI.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 04.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
class ApplicationAPI {
    
    
    func autorization(login: String, password: String,
                      completion: @escaping (_ success: Bool)-> Void){
        let url = URL(string: "https://four-styles-app.herokuapp.com/api/authenticate")
        var request = URLRequest(url: url!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONEncoder().encode(LoginRequest(username: login, password: password))
        
        Alamofire.request(request).response { (response) in
//            print(response)
            if(response.response?.statusCode == 200){
                guard let loginResponse = try? JSONDecoder().decode(LoginResponce.self, from: response.data!) else {
                    completion(false)
                    return
                }
                TokenManager.token = "Bearer \(loginResponse.token)"
                self.getUserId(completion: completion)
            } else {
                completion(false)
            }
        }
    }
    
    func getUserId(completion: @escaping (_ success: Bool)-> Void){
        let url = "https://four-styles-app.herokuapp.com/api/account"
        let header = ["Authorization": TokenManager.token]
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
//            print(response)
            if(response.response?.statusCode == 200){
                if let value = response.result.value as? [String: Any]{
                    TokenManager.idUser = value["id"] as? Int
                    completion(true)
                } else {
                    completion(false)
                }
            } else {
                completion(false)
            }
        }
    }
    
    func getWardrobesUser(completion: @escaping (_ success: [Wardrobes]?)-> Void){
        let url = "https://four-styles-app.herokuapp.com/api/users/\(TokenManager.idUser!)/wardrobes"
        let header = ["Authorization": TokenManager.token]
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            //            print(response)
            if(response.response?.statusCode == 200){
                guard let wardrobes = try? JSONDecoder().decode([Wardrobes].self, from: response.data!) else {
                    completion(nil)
                    return
                }
                completion(wardrobes)
            } else {
                completion(nil)
            }
        }
    }
    
    func getWardrobesCloths(idWardrobes: Int,completion: @escaping (_ success: [Cloth]?)-> Void){
        let url = "https://four-styles-app.herokuapp.com/api/wardrobes/\(idWardrobes)/cloths"
        let header = ["Authorization": TokenManager.token]
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            print(response)
            if(response.response?.statusCode == 200){
                guard let wardrobes = try? JSONDecoder().decode([Cloth].self, from: response.data!) else {
                    completion(nil)
                    return
                }
                completion(wardrobes)
            } else {
                completion(nil)
            }
        }
    }
    func udateCloth(cloth:Cloth, completion: @escaping (_ success: Bool?)->Void ){
        let url = URL(string: "https://four-styles-app.herokuapp.com/api/cloths")
        var request = URLRequest(url: url!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONEncoder().encode(cloth)
        request.setValue(TokenManager.token, forHTTPHeaderField: "Authorization")
        
        Alamofire.request(request).response { (response) in
            print(response)
            if(response.response?.statusCode == 200){
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func addCloths(cloths: [ClothRequest], completion: @escaping (_ success: Bool?)->Void){
        let url = URL(string: "https://four-styles-app.herokuapp.com/api/cloths/batch")
        var request = URLRequest(url: url!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONEncoder().encode(cloths)
        request.setValue(TokenManager.token, forHTTPHeaderField: "Authorization")
        
        Alamofire.request(request).response { (response) in
            print(response)
            if(response.response?.statusCode == 200){
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    func addFavouriStyle(style: Style, completion: @escaping (_ success: Bool?)->Void){
        let url = URL(string: "https://four-styles-app.herokuapp.com/api/styles")
        var styl = style
        styl.userId = TokenManager.idUser
        var request = URLRequest(url: url!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONEncoder().encode(styl)
        request.setValue(TokenManager.token, forHTTPHeaderField: "Authorization")
        
        Alamofire.request(request).response { (response) in
            print(response)
            if(response.response?.statusCode == 200){
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    func getFaivoryStayle(completion: @escaping (_ success: [Style]?)-> Void){
        let url = "https://four-styles-app.herokuapp.com/api/users/\(TokenManager.idUser!)/styles"
        let header = ["Authorization": TokenManager.token]
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            print(response)
            if(response.response?.statusCode == 200){
                guard let styles = try? JSONDecoder().decode([Style].self, from: response.data!) else {
                    completion(nil)
                    return
                }
                completion(styles)
            } else {
                completion(nil)
            }
        }
    }
    
    func generationStyle(idWardrobes: Int,completion: @escaping (_ success: [Style]?)-> Void){
        let url = "https://four-styles-app.herokuapp.com/api/wardrobes/\(idWardrobes)/styles"
        let header = ["Authorization": TokenManager.token]
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            print(response)
            if(response.response?.statusCode == 200){
                guard let wardrobes = try? JSONDecoder().decode([Style].self, from: response.data!) else {
                    completion(nil)
                    return
                }
                completion(wardrobes)
            } else {
                completion(nil)
            }
        }
    }
    
    func uploadFoto(image: UIImage,completion: @escaping (_ success: String?)-> Void){
        let url = URL(string: "https://four-styles-app.herokuapp.com/api/uploads")
        
        let headers: HTTPHeaders
        headers = ["Content-type": "multipart/form-data",
                   "Content-Disposition" : "form-data",
                   "Authorization":TokenManager.token]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in headers {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

            if let data = image.pngData(){
                multipartFormData.append(data, withName: "file", fileName: "image.png", mimeType: "image/jpeg")
            }

        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            print(result)
            switch result{
            case .success(let upload, _, _):
                upload.responseString { (response) in
                    guard let url = response.result.value else {
                        completion(nil)
                        return
                    }
                    print(url)
                    completion(url)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
}

