//
//  GenerateStyleViewController.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 16.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import UIKit

class GenerateStyleViewController: UIViewController {
    var presenter: GenerateStylePresenter!
    var style = [Style]()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = GenerateStylePresenter(self)
        navigationItem.title = "Сгенерированные стили"
        // Do any additional setup after loading the view.
    }
    

}
extension GenerateStyleViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        style.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var cell : UITableViewCell!
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = style[indexPath.row].name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "styleInfo") as! StyleInfoViewController
        controller.style = style[indexPath.row]
        controller.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
