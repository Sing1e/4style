//
//  WardrobesViewController.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 13.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import UIKit

class WardrobesViewController: UIViewController {
    var presenter: WardrobesPresenter!
    var wardrobes: [Wardrobes] = []
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = WardrobesPresenter(self)
        registerCell()
        presenter.onLoad()
    }
    func registerCell() {
        self.tableView.register(UINib.init(nibName: "WardrobesCell", bundle: nil), forCellReuseIdentifier: "wardrobesCell")
        self.tableView.rowHeight = UITableView.automaticDimension
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    func updateTable(_ wardrobes: [Wardrobes]){
        self.wardrobes = wardrobes
        tableView.reloadData()
    }
    
    func openStyleScreen(wardrobe: Wardrobes){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "cloths") as! ClothsViewController
        controller.wardrobe = wardrobe
        controller.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension WardrobesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wardrobes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wardrobesCell", for: indexPath) as! WardrobesCell
        cell.setInfo(wardrobes[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openStyleScreen(wardrobe: wardrobes[indexPath.row])
    }
    
    
}
