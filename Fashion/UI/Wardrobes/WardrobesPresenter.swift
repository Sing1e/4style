//
//  WardrobesPresenter.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 13.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
class WardrobesPresenter{
    var view: WardrobesViewController!
    let api = ApplicationAPI()
    init(_ view: WardrobesViewController) {
        self.view = view
    }
    
    func onLoad(){
        api.getWardrobesUser { (wardrobes) in
            if(wardrobes != nil){
                self.view.updateTable(wardrobes!)
            }
        }
    }
}
