//
//  WardrobesCell.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 13.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import UIKit

class WardrobesCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setInfo(_ wardrobes: Wardrobes){
        iconImage.image = UIImage(named: "wardrobes")
        titleLabel.text = wardrobes.name
    }
    
}
