//
//  StyleInfoPresenter.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 18.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
class StyleInfoPresenter {
    var view: StyleInfoViewController!
    let api = ApplicationAPI()
    init(_ view: StyleInfoViewController) {
        self.view = view
    }
    
    func addStyle(style: Style){
        api.addFavouriStyle(style: style) { (success) in
            print("Add styles faivory: \(success)")
        }
    }
}
