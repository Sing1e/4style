//
//  StyleInfoViewController.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 16.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import UIKit

class StyleInfoViewController: UIViewController {
    var presenter: StyleInfoPresenter!
    var style: Style!
    var faivory = true
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = StyleInfoPresenter(self)
        registerCell()
        navigationItem.title = style.name
        if(faivory){
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "В избранное", style: .plain, target: self, action: #selector(addFaivoty))
        }
    }
    func registerCell() {
        self.tableView.register(UINib.init(nibName: "ClothTableViewCell", bundle: nil), forCellReuseIdentifier: "clothCell")
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    @objc func addFaivoty(){
        presenter.addStyle(style: style)
    }
}
extension StyleInfoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return style.cloths.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "clothCell", for: indexPath) as! ClothTableViewCell
        cell.setInfo(cloth: style.cloths[indexPath.row])
        return cell
    }
    
    
}
