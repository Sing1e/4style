//
//  AvtorizationContract.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 08.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
import UIKit
protocol AvtorizationViewProtocol {
    func showAlert(title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction], handler: ((UIAlertAction) -> Void)?)
    func hideAlert(completion: @escaping () -> Void)
    func openMainScreen()
}

protocol AvtorizationPresenterProtocol {
    func enterButtonTap(login: String, password: String)
}
