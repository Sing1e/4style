//
//  AvtorizationViewController.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 04.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import UIKit
import algorithmia
import Cloudinary
class AvtorizationViewController: UIViewController {
    var presenter: AvtorizationPresenterProtocol!
    var informationAlert: UIAlertController!
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = AvtorizationPresenter(self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func enterButtonTap(_ sender: UIButton) {
        presenter.enterButtonTap(login: loginField.text!, password: passwordField.text!)
    }

}

extension AvtorizationViewController: AvtorizationViewProtocol{
    func openMainScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "tabBarController")
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func showAlert(title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction], handler: ((UIAlertAction) -> Void)?) {
        informationAlert = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions {
            informationAlert.addAction(action)
        }
        self.present(informationAlert, animated: true, completion: nil)
    }
    
    func hideAlert(completion: @escaping () -> Void) {
        informationAlert.dismiss(animated: false) {
            completion()
        }
    }
}

