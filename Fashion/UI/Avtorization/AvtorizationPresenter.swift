//
//  AvtorizationPresenter.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 08.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
import UIKit
class AvtorizationPresenter: AvtorizationPresenterProtocol{
    var view: AvtorizationViewProtocol!
    let api = ApplicationAPI()
    init(_ view: AvtorizationViewProtocol) {
        self.view = view
    }
    func enterButtonTap(login: String, password: String) {
        view.showAlert(title: nil, message: "Авторизация...", style: .alert, actions: [], handler: nil)
        api.autorization(login: login, password: password) { (success) in
            self.view.hideAlert {
                if(success){
                    self.view.openMainScreen()
                } else {
                    let action = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
                    self.view.showAlert(title: "Ошибка авторизации", message: nil, style: .alert, actions: [action], handler: nil)
                }
            }
        }
    }
    
    
}
