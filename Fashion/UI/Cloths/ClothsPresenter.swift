//
//  ClothsPresenter.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 13.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
import algorithmia
import UIKit
class ClothsPresenter {
    var view: ClothsViewController!
    let api = ApplicationAPI()
    let lose = [18,27,26]
    var wardropeId: Int!
    init(_ view: ClothsViewController, wardropId: Int) {
        self.view = view
        self.wardropeId = wardropId
    }
    
    func onLoad(id: Int){
        api.getWardrobesCloths(idWardrobes: id) { (cloth) in
            if(cloth != nil){
                self.view.updateTable(cloth!)
            }
        }
    }
    func updateCloth(id: Int, cloth: Cloth){
        api.udateCloth(cloth: cloth) { (success) in
            if(!success!){
                self.view.enabledCloth(id: id, value: !cloth.available!)
            }
        }
    }
    func generateStyle(id: Int){
        view.showAlert(title: "Идет генерация стиля", message: nil, style: .alert, actions: [], handler: nil)
        api.generationStyle(idWardrobes: id) { (styles) in
            self.view.hideAlert {
                self.view.openStyles(styles: styles ?? [])
            }
            
        }
    }
    
    func uploadFile(image: UIImage){
        view.showAlert(title: "Обработка фотографии", message: nil, style: .alert, actions: [], handler: nil)
        api.uploadFoto(image: image) { (url) in
            if(url != nil){
                let body: [String : Any] = [
                    "image": url,
                    "model": "large",
                    "tags_only": true]
                let jsonData = try! JSONSerialization.data(withJSONObject: body)
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
                let client = Algorithmia.client(simpleKey: "simgGJ2+6QrcuuI7a8ivAqAjvDz1")
                let algo = client.algo(algoUri: "algorithmiahq/DeepFashion/1.3.0")
                print("start algoritmia")
                algo.pipe(rawJson: jsonString! as String) { resp, error in
                    print(resp.getJson() )
                    guard let json = try? JSONSerialization.data(withJSONObject: resp.getJson(), options: []) else {
                        self.view.hideAlert {}
                        return
                    }
                    guard let articles = try? JSONDecoder().decode(Articles.self, from: json) else {
                        self.view.hideAlert {}
                        return
                    }
                    self.sortArticle(image: image, articles: articles)
                }
            }
        }
    }
    
    func sortArticle(image: UIImage, articles: Articles){
        var cloths: [ClothRequest] = []
        for article in articles.articles {
            let type = ArticlesHelper.getArticlesType(name: article.articleName)
            if(lose.contains(type)){
                continue
            }
            let image = pruningPhoto(image: image, bound: article.boundingBox)
            var cloth = ClothRequest(available: true, clothTypeId: type, favourite: false, image: (image.pngData()?.base64EncodedString(options: .lineLength64Characters))!, wardrobeId: wardropeId)
            cloths.append(cloth)
        }
        if(cloths.count == 0){
            self.view.hideAlert {
                self.tryAgainAlert()
            }
            return
        }
        api.addCloths(cloths: cloths) { (success) in
            self.view.hideAlert {
                self.onLoad(id: self.wardropeId)
            }
        }
    }
    
    func pruningPhoto(image: UIImage, bound: BoundingBox) -> UIImage{
        let rect = CGRect(x: bound.x0, y: bound.y0, width: bound.x1 - bound.x0, height: bound.y1 - bound.y0)
        return cropImage(image: image, toRect: rect)
    }
    
    func cropImage(image:UIImage, toRect rect:CGRect) -> UIImage{
        let imageRef:CGImage = image.cgImage!.cropping(to: rect)!
        let croppedImage:UIImage = UIImage(cgImage:imageRef)
        return croppedImage
    }
    
    func tryAgainAlert(){
        let button = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        self.view.showAlert(title: "Не удалось определить вещи", message: "Попробуйте еще раз", style: .alert, actions: [button], handler: nil)
    }
}
