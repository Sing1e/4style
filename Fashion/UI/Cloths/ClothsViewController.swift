//
//  ClothsViewController.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 13.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import UIKit
import Photos
class ClothsViewController: UIViewController {
    var presenter: ClothsPresenter!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var wardrobe: Wardrobes!
    var cloths: [Cloth] = []
    
    var informationAlert: UIAlertController!
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        presenter = ClothsPresenter(self, wardropId: wardrobe.id)
        titleLabel.text = wardrobe.name
        setNavigationBar()
        presenter.onLoad(id: wardrobe.id)
        // Do any additional setup after loading the view.
    }
    func setNavigationBar(){
        navigationItem.title = "Стиль"
        let button = UIBarButtonItem(title: "Добавить", style: .done, target: self, action: #selector(addCloth))
        navigationItem.rightBarButtonItem = button
    }
    func registerCell() {
        self.tableView.register(UINib.init(nibName: "ClothTableViewCell", bundle: nil), forCellReuseIdentifier: "clothCell")
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    func updateTable(_ cloths: [Cloth]){
        self.cloths = cloths
        tableView.reloadData()
    }
    
    @IBAction func generateStylesButtonTap(_ sender: UIButton) {
        presenter.generateStyle(id: wardrobe.id)
    }
    
    @objc func addCloth(){
        let buttonCamera = UIAlertAction(title: "Камера", style: .default) { (action) in
            self.choisePhoto(type: .camera)
        }
        let buttonPhoto = UIAlertAction(title: "Фото", style: .default) { (action) in
            self.choisePhoto(type: .photoLibrary)
        }
        let buttonCancel = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        showAlert(title: "Выберите действие", message: nil, style: .actionSheet, actions: [buttonCancel,buttonPhoto,buttonCamera], handler: nil)
    }
    
    func choisePhoto(type: UIImagePickerController.SourceType){
        imagePicker.delegate = self
        imagePicker.sourceType = type
        imagePicker.allowsEditing = false
        
        present(imagePicker, animated: true, completion: nil)
    }
    func showAlert(title: String?, message: String?, style: UIAlertController.Style, actions: [UIAlertAction], handler: ((UIAlertAction) -> Void)?) {
        informationAlert = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions {
            informationAlert.addAction(action)
        }
        self.present(informationAlert, animated: true, completion: nil)
    }
    func enabledCloth(id: Int, value: Bool){
        cloths[id].available = value
    }
    func hideAlert(completion: @escaping () -> Void) {
        informationAlert.dismiss(animated: false) {
            completion()
        }
    }
    
    func openStyles(styles: [Style]){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "generateStyle") as! GenerateStyleViewController
        controller.style = styles
        controller.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension ClothsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cloths.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "clothCell", for: indexPath) as! ClothTableViewCell
        cell.setInfo(cloth: cloths[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var action: UITableViewRowAction!
        if(cloths[indexPath.row].available!){
            action = UITableViewRowAction(style: .destructive, title: "В стирку", handler: { (action, index) in
                self.cloths[index.row].available = false
                self.presenter.updateCloth(id: indexPath.row, cloth: self.cloths[indexPath.row])
            })
        } else {
            action = UITableViewRowAction(style: .default, title: "В гардероб", handler: { (action, index) in
                self.cloths[index.row].available = true
                self.presenter.updateCloth(id: indexPath.row, cloth: self.cloths[indexPath.row])
            })
            action.backgroundColor = UIColor.blue
        }
        return [action]
    }
}

extension ClothsViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            dismiss(animated: true, completion: {
                if(pickedImage.pngData()!.count / 1024 >= 5){
                    let image = pickedImage.resized(withPercentage: 0.1)
                    self.presenter.uploadFile(image: image!)
                } else {
                    self.presenter.uploadFile(image: pickedImage)
                }
                })
        }
    }
    
}
