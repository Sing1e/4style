//
//  FaivoryPresenter.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 18.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
class FaivoryPresenter {
    var view: FaivoryViewController!
    let api = ApplicationAPI()
    init(_ view: FaivoryViewController) {
        self.view = view
    }
    
    func onLoad(){
        api.getFaivoryStayle { (style) in
            if(style != nil){
                self.view.showStyle(style: style!)
            }
        }
    }
}
