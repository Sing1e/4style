//
//  FaivoryViewController.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 18.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import UIKit

class FaivoryViewController: UIViewController {
    var presenter: FaivoryPresenter!
    var style = [Style]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = FaivoryPresenter(self)
        presenter.onLoad()
    }
    
    func showStyle(style: [Style]){
        self.style = style
        tableView.reloadData()
    }

}

extension FaivoryViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        style.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var cell : UITableViewCell!
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = style[indexPath.row].name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "styleInfo") as! StyleInfoViewController
        controller.style = style[indexPath.row]
        controller.faivory = false
        controller.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
