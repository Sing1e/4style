//
//  AttachmentHandler.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 16.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos

class AttachmentHandler: NSObject {
    static let shared = AttachmentHandler()
    fileprivate var currentVC: UIViewController?

    //MARK: - Internal Properties
    var filePickedBlock: ((URL) -> Void)?
    var imageURLBlock: ((URL) -> Void)?

    enum AttachmentType: String {
        case camera, photoLibrary
    }

    //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = "Прикрепить файл"
        static let actionFileTypeDescription = "Выберите тип файла для добавления..."
        static let camera = "Камера"
        static let phoneLibrary = "Фото"
        static let file = "Файл"

        static let alertForPhotoLibraryMessage = "У приложения нет доступа к Вашим фото. Чтобы разрешить доступ, зайдите в Настройки, выберите \"Транскрипт\" и предоставьте доступ к медиатеке."

        static let alertForCameraAccessMessage = "У приложения нет доступа к Вашей камере. Чтобы разрешить доступ, зайдите в Настройки, выберите \"Транскрипт\" и предоставьте доступ к камере."

        static let settingsBtnTitle = "Настройки"
        static let cancelBtnTitle = "Отмена"
    }

    //MARK: - showAttachmentActionSheet
    // This function is used to show the attachment sheet for image, photo and file.
    func showAttachmentActionSheet(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))

        actionSheet.addAction(UIAlertAction(title: Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }))

        if #available(iOS 11.0, *) {
            actionSheet.addAction(UIAlertAction(title: Constants.file, style: .default, handler: { (action) -> Void in
                self.documentPicker()
            }))
        }
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))

        actionSheet.popoverPresentationController?.sourceView = vc.view
        actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: vc.view.bounds.midX, y: vc.view.bounds.midY, width: 0, height: 0)

        vc.present(actionSheet, animated: true, completion: nil)
    }

    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController) {
        currentVC = vc

        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera {
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary {
                photoLibrary()
            }
        case .denied:
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized {
                    // photo library access given
                    if attachmentTypeEnum == AttachmentType.camera {
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photoLibrary {
                        self.photoLibrary()
                    }
                } else {
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }


    //MARK: - CAMERA PICKER
    //This function is used to open camera from the iphone
    func openCamera() {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch status {
        case .authorized:
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .camera
                currentVC?.present(myPickerController, animated: true, completion: nil)
            }
        case .denied:
            self.addAlertForSettings(AttachmentType.camera)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                if response {
                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        let myPickerController = UIImagePickerController()
                        myPickerController.delegate = self
                        myPickerController.sourceType = .camera
                        self.currentVC?.present(myPickerController, animated: true, completion: nil)
                    }
                } else {
                    self.addAlertForSettings(AttachmentType.camera)
                }
            }
        case .restricted:
            self.addAlertForSettings(AttachmentType.camera)
        default:
            break
        }
    }

    //MARK: - PHOTO PICKER
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }

    //MARK: - FILE PICKER
    func documentPicker() {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),
                                                                      String(kUTTypeXML),
                                                                      String(kUTTypeCompositeContent),
                                                                      String(kUTTypeText),
                                                                      String(kUTTypePlainText),
                                                                      String(kUTTypeZipArchive),
                                                                      String(kUTTypeJPEG),
                                                                      "com.microsoft.word.doc",
                                                                      "org.openxmlformats.wordprocessingml.document",
                                                                      "com.microsoft.excel.xls"], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        
        importMenu.popoverPresentationController?.sourceView = currentVC?.view
        importMenu.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        importMenu.popoverPresentationController?.sourceRect = CGRect(x: currentVC?.view.bounds.midX ?? 0, y: currentVC?.view.bounds.midY ?? 0, width: 0, height: 0)
        currentVC?.present(importMenu, animated: true, completion: nil)
    }

    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType) {
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera {
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary {
            alertTitle = Constants.alertForPhotoLibraryMessage
        }

        let cameraUnavailableAlertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController.addAction(cancelAction)
        cameraUnavailableAlertController.addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController, animated: true, completion: nil)
    }
}

//MARK: - IMAGE PICKER DELEGATE
// This is responsible for image picker interface to access image and then responsibel for canceling the picker
extension AttachmentHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        let imgData: NSData = (selectedImage.jpegData(compressionQuality: CGFloat(1))! as NSData)
        let imageSize: Int = imgData.length / 1024
        if picker.sourceType == .camera {
            let imageName = "Снимок " + self.getTimeStamp()
            let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(imageName).jpeg"
            let imageUrl: URL = URL(fileURLWithPath: imagePath)
            let builtInCompressionCoefficient = 3.0
            try? selectedImage.jpegData(compressionQuality: CGFloat(builtInCompressionCoefficient/(Double(imageSize)/1024.0)))?.write(to: imageUrl)
            self.imageURLBlock?(imageUrl)
        } else {
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let imageUrl          = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL
            let imageName         = "Фото " + self.getTimeStamp() + "." + (imageUrl?.pathExtension ?? "jpeg")
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let photoURL          = NSURL(fileURLWithPath: documentDirectory)
            let localPath         = photoURL.appendingPathComponent(imageName)
            if !FileManager.default.fileExists(atPath: localPath!.path) {
                do {
                    try image.jpegData(compressionQuality: 1.0)?.write(to: localPath!)
                    self.imageURLBlock?(localPath!)
                    print("file saved")
                } catch {
                    print("error saving file")
                }
            }
        }
        currentVC?.dismiss(animated: true, completion: nil)
    }

    func getTimeStamp() -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        return dateFormatter.string(from: date)
    }
}

//MARK: - FILE IMPORT DELEGATE
extension AttachmentHandler: UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        currentVC?.present(documentPicker, animated: true, completion: nil)
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        self.filePickedBlock?(url)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


