//
//  ArticlesHelper.swift
//  Fashion
//
//  Created by Alexandr Shevchenko on 17.12.2019.
//  Copyright © 2019 Alexandr Shevchenko. All rights reserved.
//

import Foundation
class ArticlesHelper {
    static func getArticlesType(name: String) -> Int {
        switch name {
        case "top handle bag":
            return 1
        case "t shirt":
            return 2
        case "jewelry":
            return 3
        case "boots":
            return 4
        case "sunglasses":
            return 5
        case "jeans":
            return 6
        case "sweater":
            return 7
        case "tank top":
            return 8
        case "skirt":
            return 9
        case "sandals":
            return 10
        case "leggings":
            return 11
        case "button down shirt":
            return 12
        case "pants casual":
            return 13
        case "heels pumps or wedges":
            return 14
        case "lingerie":
            return 15
        case "blouse":
            return 16
        case "lightweight jacket":
            return 17
        case "casual dress":
            return 18
        case "winter jacket":
            return 19
        case "formal dress":
            return 20
        case "watches":
            return 21
        case "hat":
            return 22
        case "vest":
            return 23
        case "sneakers":
            return 24
        case "houlder bag":
            return 25
        case "flats":
            return 26
        case "overall":
            return 27
        case "sweatpants":
            return 28
        case "shorts":
            return 29
        case "rompers":
            return 30
        case "pants suit formal":
            return 31
        case "glasses":
            return 32
        case "clutches":
            return 33
        case "socks":
            return 34
        case "backpack or messenger bag":
            return 35
        case "jumpsuit":
            return 36
        case "running shoes":
            return 37
        case "blazer":
            return 38
        case "tunic":
            return 39
        case "hosiery":
            return 40
        case "denim jacket":
            return 41
        case "belts":
            return 42
        case "43":
            return 43
        case "trenchcoat":
            return 44
        case "headwrap":
            return 45
        case "sweater dress":
            return 46
        case "sweatshirt":
            return 47
        case "gloves":
            return 48
        case "underwear":
            return 49
        default:
            return 1
        }
    }
}
